package vendingMachine;

import java.util.ArrayList;
import java.util.List;

public class VendingMachine {
	public double balance;
    public ArrayList<Item> items;

    public VendingMachine(double balance)
    {
        this.balance = balance;
        this.items = new ArrayList();
    }

    public void buyItem(Item item)
    {
        int index;
        if((index = indexOfItem(this.items, item)) > -1)
        {
            this.items.get(index).count += item.count;
            this.balance -= (item.count * item.buyPrice);
        } else {
            this.items.add(item);
            this.balance -= (item.count * item.buyPrice);
        }
    }

    public void sellItem(String name, int count) throws ArrayIndexOutOfBoundsException
    {
        int index = indexOfItem(this.items, name);
        if(index > -1)
        {
            if(count <= this.items.get(index).count) {
                this.items.get(index).countSold += count;
                this.items.get(index).count -= count;
                this.balance += (count * this.items.get(index).sellPrice);
            } else {
                this.items.get(index).countSold += this.items.get(index).count;
                this.items.get(index).count = 0;
                this.balance += (this.items.get(index).count * this.items.get(index).sellPrice);
            }
        } else {
            throw new ArrayIndexOutOfBoundsException(index);
        }
    }

    public int totalSales()
    {
        int sales = 0;
        for(int i = 0; i < this.items.size(); i++)
        {
            sales += this.items.get(i).countSold;
        }

        return sales;
    }

    private static int indexOfItem(List<Item> items, Item item)
    {
        int index = -1;
        for(int i = 0; i < items.size(); i++)
        {
            if(items.get(i).name == item.name)
            {
                index = i;
                break;
            }
        }

        return index;
    }
    
    private static int indexOfItem(List<Item> items, String name)
    {
        int index = -1;
        for(int i = 0; i < items.size(); i++)
        {
            if(items.get(i).name == name)
            {
                index = i;
                break;
            }
        }

        return index;
    }
    
    public static void main(String args[])
    {
    	VendingMachine vm = new VendingMachine(10.00);
    	System.out.println(vm.balance);
    	Item chips = new Item("Chips", 1.00, 1.50, 4);
    	vm.buyItem(chips);
    	System.out.println(vm.balance);
    	vm.sellItem("Chips", 3);
    	System.out.println(vm.balance);
    	int totalSales = vm.totalSales();
    	System.out.println(totalSales);
    }

}
