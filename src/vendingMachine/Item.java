package vendingMachine;

public class Item {
	public final String name;
    public final double buyPrice, sellPrice;
    public int count, countSold;
    public Item(String name, double buyPrice, double sellPrice, int count)
    {
        this.name = name;
        this.buyPrice = buyPrice;
        this.sellPrice = sellPrice;
        this.count = count;
    }
}
